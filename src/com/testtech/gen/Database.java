package com.testtech.gen;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public final class Database {

	public final static int SCHEMA_VERSION = 1;
	public final static String SCHEMA_PACKAGE = "com.testtech.app.model";

	public static void main(String... args) {

		if (args != null && args.length == 1) {

			final Schema s = new Schema(Database.SCHEMA_VERSION,
					Database.SCHEMA_PACKAGE);
			s.enableKeepSectionsByDefault();
			s.enableActiveEntitiesByDefault();

			// Define base entities (CREATE TABLE ...)
			final Entity 
					user = Database.createUser(s),
					profile = Database.createProfile(s, user), 
					filter = Database.createFilter(s,user), 
					survey = Database.createSurvey(s, user), 
					question_type = Database.createQuestionType(s), 
					question = Database.createQuestion(s, survey), 
					option = Database.createOption(s, question);

			try {
				new DaoGenerator().generateAll(s, args[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	private static Entity createOption(Schema s, Entity question) {

		final Entity option = s.addEntity("Option");
		option.setTableName("option");
		option.implementsSerializable();

		option.addIdProperty().columnName("id").notNull();
		
		option.addToOne(question, 
					  option.addLongProperty("questionId").columnName("question_id")
					  		.notNull().getProperty()).setName("question");
		question.addToOne(option, 
						question.addLongProperty("optionId").columnName("option_id").getProperty());
		
		option.addStringProperty("flavorText").columnName("flavor_text").notNull();
		option.addStringProperty("value").columnName("value");

		return option;
	}

	private static Entity createQuestion(Schema s, Entity survey) {

		final Entity question = s.addEntity("Question");
		question.setTableName("question");
		question.implementsSerializable();

		question.addIdProperty().columnName("id").notNull();
		
		question.addToOne(survey, 
					  question.addLongProperty("surveyId").columnName("survey_id")
					  		.notNull().getProperty()).setName("survey");
		survey.addToOne(question, 
						survey.addLongProperty("questionId").columnName("question_id").getProperty());
		
		question.addLongProperty("typeId").columnName("type_id").notNull();
		question.addStringProperty("flavorText").columnName("flavor_text").notNull();
		question.addBooleanProperty("required").columnName("required").notNull();

		return question;
	}

	private static Entity createQuestionType(Schema s) {

		final Entity type = s.addEntity("QuestionType");
		type.setTableName("question_type");
		type.implementsSerializable();

		type.addIdProperty().columnName("id").notNull();
		type.addStringProperty("type").columnName("type").notNull();
		type.addStringProperty("description").columnName("description")
				.notNull();

		return type;
	}

	private static Entity createSurvey(Schema s, Entity user) {

		final Entity survey = s.addEntity("Survey");
		survey.setTableName("survey");
		survey.implementsSerializable();

		survey.addIdProperty().columnName("id").notNull();
		survey.addStringProperty("title").columnName("title").notNull();
		survey.addStringProperty("description").columnName("description")
				.notNull();
		survey.addStringProperty("author").columnName("author").notNull();
		survey.addBooleanProperty("multipleAttemptsPermitted")
				.columnName("multiple_attempts").notNull();
		survey.addDateProperty("creationDate").columnName("created").notNull();
		survey.addBooleanProperty("answered").columnName("answered").notNull();

		return survey;
	}

	private static Entity createFilter(Schema s, Entity user) {

		final Entity filter = s.addEntity("Filter");
		filter.setTableName("filter");
		filter.implementsSerializable();

		filter.addIdProperty().columnName("id").notNull();
		filter.addToOne(user,
				filter.addLongProperty("userId").columnName("user_id")
						.notNull().getProperty());
		filter.addStringProperty("name").columnName("name").notNull();
		filter.addStringProperty("orderingMethod")
				.columnName("ordering_method").notNull();

		return filter;
	}

	private static Entity createProfile(Schema s, Entity user) {

		final Entity profile = s.addEntity("Profile");
		profile.setTableName("profile");
		profile.implementsSerializable();

		profile.addIdProperty().columnName("id").notNull();
		profile.addStringProperty("firstName").columnName("first_name");
		profile.addStringProperty("lastName").columnName("last_name");
		profile.addDateProperty("birthDate").columnName("birth-date");
		profile.addByteProperty("gender").columnName("gender").notNull();

		profile.addToOne(
				user,
				profile.addLongProperty("userId").columnName("user_id")
						.notNull().unique().getProperty()).setName("user");
		user.addToOne(profile,
				user.addLongProperty("profileId").columnName("profile_id")
						.getProperty());

		return profile;
	}

	private static Entity createUser(Schema s) {

		final Entity user = s.addEntity("User");
		user.setTableName("user");
		user.implementsSerializable();

		user.addIdProperty().columnName("id").notNull();
		user.addStringProperty("username").columnName("username").notNull()
				.unique();
		user.addStringProperty("email").columnName("email").notNull();
		user.addLongProperty("creationDate").columnName("created").notNull();
		user.addLongProperty("lastSynchronizationDate").columnName("last_sync");

		return user;
	}

}